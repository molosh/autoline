<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('WP_MEMORY_LIMIT', '128M');

define('WP_HOME',$_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME']);
define('WP_SITEURL',$_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME']);

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'autoline');

/** Имя пользователя MySQL */
define('DB_USER', 'autoline');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'autoline');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NZJC.=Db6-g@`-au{=MBfPfHZC<[86{y4h|A<O*e4x>[*JiGw^/zJl5eQM58FOoF');
define('SECURE_AUTH_KEY',  'E)V{<[,0ncl,1Vf3aXrzCQ-4C2duaO&kGb8CVM3#@x9]u8&XPta6f;}87WtV{~&8');
define('LOGGED_IN_KEY',    '|-/@9G&Yv{jZa/ZM$kp&6_(zB~63aL+-#oswcI(a_nW9L,1j4T;TMi5JF0!jSU6R');
define('NONCE_KEY',        'o>4qHn0qdjv$a)I}:>WAHvJ6FPd]U?<J6iSl(@vQEU/%p0BW&a<LTLMbNQ#8<R:z');
define('AUTH_SALT',        'OTcl,!azB?EyC#R7O33;3V&|@#07RQQhlyo>zdxKx{L%Z_s:QP=2j8CN(&Jo.W!9');
define('SECURE_AUTH_SALT', '3u8/Xy|qLh<_)p]JGR-hC%Dggx#;g$$oW?Jq[t^x*g>@~c_^VbUm675^@[s~&~t*');
define('LOGGED_IN_SALT',   '+>ly`amF26d$}3BgJ*aj15QOLqe|w;[>Y8^`OPYfv&oLr>g,Sp(yY>`](0>4+o:@');
define('NONCE_SALT',       'H%&jJM:-%WaFVkQO8v})>W`r~3MkZiU56#<]>7,>rh^s3&$9>.:+dBjuRC$yb)qU');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
