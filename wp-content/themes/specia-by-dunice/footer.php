<section class="footer-copyright">
    <div class="container">
        <div class="row ">
            <div class="text-center">
                <?php if (is_active_sidebar('footer_sidebar')) : ?>
                    <div id="footer-sidebar" class="footer-sidebar widget-area" role="complementary">
                        <?php dynamic_sidebar('footer_sidebar'); ?>
                    </div>
                <?php endif; ?>
            </div>
</section>

<a href="#" class="top-scroll"><i class="fa fa-hand-o-up"></i></a>
</div>
<?php wp_footer(); ?>
</body>
</html>


