<header role="banner">
    <nav class='navbar navbar-default <?php echo specia_sticky_menu(); ?>' role='navigation' style="position: relative">

        <div class="container">
            <div class="row">

                <div class="header-content-wrapper">
                    <div class="navbar-header">
                        <div class="header-text-wrapper">
                            <a style="color: #000 !important;" class="navbar-brand"
                               href="<?php echo esc_url(home_url('/')); ?>"
                               class="brand">
                                <?php
                                $title = get_bloginfo('title');
                                if ($title) : ?>
                                    <span><?php echo $title; ?></span>
                                <?php endif; ?>
                                <?php
                                $description = get_bloginfo('description');
                                if ($description) : ?>
                                    <p class="site-description"><?php echo $description; ?></p>
                                <?php endif; ?>
                            </a>
                        </div>
                    </div>

                    <div class="header-text-button"><?php if (is_active_sidebar('header_sidebar')) : ?>
                            <div id="header-sidebar" class="header-sidebar widget-area" role="complementary">
                                <?php dynamic_sidebar('header_sidebar'); ?>
                            </div>
                        <?php endif; ?>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only"><?php _e('Toggle navigation', 'specia'); ?></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'primary_menu',
                            'container' => '',
                            'menu_class' => 'nav navbar-nav navbar-right',
                            'fallback_cb' => 'specia_fallback_page_menu',
                            'walker' => new specia_nav_walker()
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
    </nav>
</header>
<div class="clearfix"></div>
