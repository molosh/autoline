<?php
/**
Template Name: Info
 */

get_header();?>
<section class="page-wrapper">
    <div class="container">
        <div class="row content_wrapper">
            <?php if( have_posts()) :  the_post();
                the_content();
            endif;?>
        </div>
    </div>
</section>

<?php get_footer(); ?>